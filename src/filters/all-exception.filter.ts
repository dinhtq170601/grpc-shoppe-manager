import { GrpcException } from '@exceptions/grpc.exception';
import {
  ArgumentsHost,
  Catch,
  ForbiddenException,
  HttpStatus,
  UnauthorizedException,
} from '@nestjs/common';
import { BaseRpcExceptionFilter } from '@nestjs/microservices';
import { Observable } from 'rxjs';

@Catch()
export class AllExceptionsFilter extends BaseRpcExceptionFilter {
  catch(exception: any, host: ArgumentsHost): Observable<any> {
    if (exception instanceof UnauthorizedException) {
      return super.catch(
        new GrpcException({
          message: 'Unauthorized',
          status: HttpStatus.UNAUTHORIZED,
        }),
        host,
      );
    }
    if (exception instanceof ForbiddenException) {
      return super.catch(
        new GrpcException({
          message: 'Bạn không có quyền thực hiện hay xem nội dung này',
          status: HttpStatus.FORBIDDEN,
        }),
        host,
      );
    }
    return super.catch(exception, host);
  }
}

import { GrpcException } from '@exceptions/grpc.exception';
import { HttpStatus, ValidationError, ValidationPipe } from '@nestjs/common';

export class MyValidationPipe extends ValidationPipe {
  constructor() {
    super({
      transform: true,
      exceptionFactory: (validationErrors: ValidationError[]) => {
        return new GrpcException({
          status: HttpStatus.BAD_REQUEST,
          message: validationErrors
            .map((error) => {
              return `${error.property}: ${error.value}`;
            })
            .join(', '),
          error: 'Bad Request',
        });
      },
    });
  }
}

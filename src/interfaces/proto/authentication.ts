/* eslint-disable */
import { Metadata } from "@grpc/grpc-js";
import { GrpcMethod, GrpcStreamMethod } from "@nestjs/microservices";
import { Observable } from "rxjs";
import { Admin } from "./common";

export const protobufPackage = "authentication";

export interface AdminRegister {
  phoneNumber: string;
  password?: string | undefined;
  email?: string | undefined;
  firstName: string;
  lastName: string;
  dateOfBirth: string;
}

export interface PhoneAndPassword {
  phoneNumber: string;
  password: string;
}

export interface AdminAndTokens {
  admin: Admin | undefined;
}

export const AUTHENTICATION_PACKAGE_NAME = "authentication";

export interface AdminAuthenticationServiceClient {
  register(request: AdminRegister, metadata: Metadata, ...rest: any): Observable<Admin>;

  logIn(request: PhoneAndPassword, metadata: Metadata, ...rest: any): Observable<AdminAndTokens>;
}

export interface AdminAuthenticationServiceController {
  register(request: AdminRegister, metadata: Metadata, ...rest: any): Promise<Admin> | Observable<Admin> | Admin;

  logIn(
    request: PhoneAndPassword,
    metadata: Metadata,
    ...rest: any
  ): Promise<AdminAndTokens> | Observable<AdminAndTokens> | AdminAndTokens;
}

export function AdminAuthenticationServiceControllerMethods() {
  return function (constructor: Function) {
    const grpcMethods: string[] = ["register", "logIn"];
    for (const method of grpcMethods) {
      const descriptor: any = Reflect.getOwnPropertyDescriptor(constructor.prototype, method);
      GrpcMethod("AdminAuthenticationService", method)(constructor.prototype[method], method, descriptor);
    }
    const grpcStreamMethods: string[] = [];
    for (const method of grpcStreamMethods) {
      const descriptor: any = Reflect.getOwnPropertyDescriptor(constructor.prototype, method);
      GrpcStreamMethod("AdminAuthenticationService", method)(constructor.prototype[method], method, descriptor);
    }
  };
}

export const ADMIN_AUTHENTICATION_SERVICE_NAME = "AdminAuthenticationService";

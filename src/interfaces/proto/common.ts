/* eslint-disable */

export const protobufPackage = "common";

export interface Admin {
  id: number;
  password: string;
  firstName: string;
  lastName: string;
  phoneNumber: string;
  createdAt: string;
  updatedAt: string;
  hashedRefreshToken: string;
  email?: string | undefined;
  age?: string | undefined;
  address?: string | undefined;
  number?: number | undefined;
  avatar?: string | undefined;
  isEmailConfirmed: boolean;
}

export const COMMON_PACKAGE_NAME = "common";

import { Admin } from '@entities/admin.entity';
import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';

@Injectable()
export class AuthenService {
  static SALT = 12;
  constructor(
    @InjectRepository(Admin)
    private readonly adminRepository: Repository<Admin>,
  ) {}

  async getAuthenticateAdmin(phoneNumber: string): Promise<Admin> {
    return await this.adminRepository.findOne({
      where: {
        phoneNumber,
        isActive: true,
      },
    });
  }
}

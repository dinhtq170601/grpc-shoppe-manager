import { formatISO } from 'date-fns';

export class Transformation {
  static mapDateToString(value?: Date): string {
    if (typeof value === 'string') {
      return value;
    }
    return formatISO(value);
  }
}

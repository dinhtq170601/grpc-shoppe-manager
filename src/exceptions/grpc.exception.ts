import { HttpStatus } from '@nestjs/common';
import { RpcException } from '@nestjs/microservices';

export class GrpcException extends RpcException {
  constructor({
    status,
    message,
    error,
  }: {
    status: HttpStatus;
    message: string;
    error?: any;
  }) {
    super(JSON.stringify({ status, message, error }));
  }
}

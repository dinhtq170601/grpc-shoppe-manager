import { AuthenticationController } from '@controllers/authentication.controller';
import { Admin } from '@entities/admin.entity';
import { Module } from '@nestjs/common';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AuthenService } from '@services/admin/authentication.service';
import { JwtModule } from '@nestjs/jwt';

@Module({
  imports: [
    JwtModule.register({}),
    ConfigModule,
    TypeOrmModule.forFeature([Admin]),
  ],
  providers: [AuthenService, ConfigService],
  controllers: [AuthenticationController],
})
export class AuthenticationModule {}

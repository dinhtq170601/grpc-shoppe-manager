import { Transformation } from '@interceptors/transformation.helper';
import { Exclude, Transform } from 'class-transformer';
import {
  Column,
  CreateDateColumn,
  Entity,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';

@Entity({ name: 'admin' })
export class Admin {
  @PrimaryGeneratedColumn('identity', {
    name: 'id',
  })
  id: number;

  @Column({ nullable: true, name: 'phone_number' })
  phoneNumber: string;

  @Column({ nullable: true, name: 'first_name' })
  firstName: string;

  @Column({ nullable: true })
  email?: string;

  @Column({ nullable: true, name: 'last_name' })
  lastName: string;

  @Column({ nullable: true })
  age?: string;

  @Column({ nullable: true })
  address?: string;

  @Column({ nullable: true })
  password: string;

  @Column({ name: 'avatar_url', nullable: true })
  avatar?: string;

  @Column({ name: 'dob', nullable: true })
  dateOfBirth?: Date;

  @Column({ name: 'hashed_refreshed_token', length: 100, nullable: true })
  @Exclude()
  hashedRefreshToken: string;

  @Column({ name: 'is_phone_number_confirmed', default: false })
  isPhoneNumberConfirmed: boolean;

  @Column({ name: 'is_email_confirmed', default: false })
  public isEmailConfirmed: boolean;

  @Column({ nullable: true, name: 'is_active', default: true })
  isActive: boolean;

  @Transform(({ value }) => Transformation.mapDateToString(value))
  @CreateDateColumn({
    name: 'created_date',
    type: 'timestamp without time zone',
  })
  createdAt: string;

  @Transform(({ value }) => Transformation.mapDateToString(value))
  @UpdateDateColumn({
    name: 'modified_date',
    type: 'timestamp without time zone',
  })
  updatedAt: string;
}

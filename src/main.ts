import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { ConfigService } from '@nestjs/config';
import { readdirSync } from 'fs';
import { MyValidationPipe } from '@pipes/my-validation.pipe';
import { join } from 'path';
import { AllExceptionsFilter } from '@filters/all-exception.filter';
import { TransformResponseInterceptor } from '@interceptors/transform-reponse.interceptor';
import { MicroserviceOptions, Transport } from '@nestjs/microservices';
import { AUTHENTICATION_PACKAGE_NAME } from '@interfaces/proto/authentication';

async function bootstrap(): Promise<void> {
  const app = await NestFactory.create(AppModule);
  const configService = app.get(ConfigService);

  const protoPath = '../../node_modules/@shoppe/shoppe-proto/proto';
  const protoFiles = readdirSync(join(__dirname, protoPath));

  app.useGlobalFilters(new AllExceptionsFilter());
  app.useGlobalPipes(new MyValidationPipe());
  app.useGlobalInterceptors(new TransformResponseInterceptor());

  app.connectMicroservice<MicroserviceOptions>({
    transport: Transport.GRPC,
    options: {
      protoPath: protoFiles.map((protoFiles) =>
        join(__dirname, `${protoPath}/${protoFiles}`),
      ),
      url: configService.get('GRPC_CONNECTION_URL'),
      package: [AUTHENTICATION_PACKAGE_NAME],
    },
  });
  await (await app.startAllMicroservices()).listen(4001);
}
bootstrap();

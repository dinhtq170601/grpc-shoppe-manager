import { LoginDto } from '@dtos/authentication.dto';
import { GrpcException } from '@exceptions/grpc.exception';
import {
  ADMIN_AUTHENTICATION_SERVICE_NAME,
  AdminAndTokens,
} from '@interfaces/proto/authentication';
import { Controller, HttpStatus } from '@nestjs/common';
import { GrpcMethod, Payload } from '@nestjs/microservices';
import { AuthenService } from '@services/admin/authentication.service';
import * as bcrypt from 'bcrypt';

@Controller()
export class AuthenticationController {
  constructor(private authenService: AuthenService) {}

  @GrpcMethod(ADMIN_AUTHENTICATION_SERVICE_NAME, 'LogIn')
  async LogIn(@Payload() dto: LoginDto): Promise<AdminAndTokens> {
    const admin = await this.authenService.getAuthenticateAdmin(
      dto.phoneNumber,
    );

    if (!admin) {
      throw new GrpcException({
        status: HttpStatus.NOT_FOUND,
        message: 'Số điện thoại hoặc mật khẩu không tồn tại',
      });
    }

    const valiPassword = bcrypt.compareSync(dto.password, admin.password);
    if (!valiPassword) {
      throw new GrpcException({
        status: HttpStatus.BAD_REQUEST,
        message: 'Số điện thoại hoặc mật khẩu bị sai',
      });
    }

    return { admin };
  }
}
